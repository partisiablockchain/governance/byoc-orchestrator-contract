package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.InteractionBuilder;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.sys.DeployBuilder;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.Governance;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.PluginInteractionCreator;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * Decorator for SysContractContextTest. This decorator makes it possible to inspect the result of
 * performing multiple calls to context.getInvocationCreator().upgradeSystemContract, which is
 * needed to test the update logic of onCreate and UPDATE_CONTRACT_BUNDLE on the BYOC orchestrator.
 */
final class SysContractContextTestDecorator implements SysContractContext {

  private final SysContractContextTest decorated;
  private final List<UpdateContractEvent> updates;

  public SysContractContextTestDecorator(SysContractContextTest decorated) {
    this.decorated = decorated;
    this.updates = new ArrayList<>();
  }

  public List<UpdateContractEvent> getUpdates() {
    return updates;
  }

  @Override
  public SystemEventCreator getInvocationCreator() {
    return new SystemEventCreatorTest(updates::add);
  }

  @Override
  public SystemEventManager getRemoteCallsCreator() {
    return decorated.getRemoteCallsCreator();
  }

  @Override
  public void setResult(DataStreamSerializable result) {
    decorated.setResult(result);
  }

  @Override
  public StateSerializable getGlobalAccountPluginState() {
    return decorated.getGlobalAccountPluginState();
  }

  @Override
  public PluginInteractionCreator getAccountPluginInteractions() {
    return decorated.getAccountPluginInteractions();
  }

  @Override
  public StateSerializable getGlobalConsensusPluginState() {
    return decorated.getGlobalConsensusPluginState();
  }

  @Override
  public PluginInteractionCreator getConsensusPluginInteractions() {
    return decorated.getConsensusPluginInteractions();
  }

  @Override
  public Governance getGovernance() {
    return decorated.getGovernance();
  }

  @Override
  public String getFeature(String key) {
    return decorated.getFeature(key);
  }

  @Override
  public void registerDeductedByocFees(
      Unsigned256 amount, String symbol, FixedList<BlockchainAddress> nodes) {
    decorated.registerDeductedByocFees(amount, symbol, nodes);
  }

  @Override
  public BlockchainAddress getContractAddress() {
    return decorated.getContractAddress();
  }

  @Override
  public long getBlockTime() {
    return decorated.getBlockTime();
  }

  @Override
  public long getBlockProductionTime() {
    return decorated.getBlockProductionTime();
  }

  @Override
  public BlockchainAddress getFrom() {
    return decorated.getFrom();
  }

  @Override
  public Hash getCurrentTransactionHash() {
    return decorated.getCurrentTransactionHash();
  }

  @Override
  public Hash getOriginalTransactionHash() {
    return decorated.getOriginalTransactionHash();
  }

  @Override
  public void payInfrastructureFees(long gas, BlockchainAddress target) {
    decorated.payInfrastructureFees(gas, target);
  }

  @Override
  public void payServiceFees(long gas, BlockchainAddress target) {
    decorated.payServiceFees(gas, target);
  }

  static final class SystemEventCreatorTest implements SystemEventCreator {

    private final Consumer<UpdateContractEvent> updateTracker;

    public SystemEventCreatorTest(Consumer<UpdateContractEvent> updateTracker) {
      this.updateTracker = updateTracker;
    }

    @Override
    public DeployBuilder deployContract(BlockchainAddress contract) {
      return null;
    }

    @Override
    public InteractionBuilder invoke(BlockchainAddress contract) {
      return null;
    }

    @Override
    public void createAccount(BlockchainAddress address) {}

    @Override
    public void createShard(String shardId) {}

    @Override
    public void removeShard(String shardId) {}

    @Override
    public void updateLocalAccountPluginState(LocalPluginStateUpdate update) {}

    @Override
    public void updateContextFreeAccountPluginState(String shardId, byte[] rpc) {}

    @Override
    public void updateGlobalAccountPluginState(GlobalPluginStateUpdate update) {}

    @Override
    public void updateAccountPlugin(byte[] pluginJar, byte[] rpc) {}

    @Override
    public void updateLocalConsensusPluginState(LocalPluginStateUpdate update) {}

    @Override
    public void updateGlobalConsensusPluginState(GlobalPluginStateUpdate update) {}

    @Override
    public void updateConsensusPlugin(byte[] pluginJar, byte[] rpc) {}

    @Override
    public void updateRoutingPlugin(byte[] pluginJar, byte[] rpc) {}

    @Override
    public void removeContract(BlockchainAddress contract) {}

    @Override
    public void exists(BlockchainAddress address) {}

    @Override
    public void setFeature(String key, String value) {}

    @Override
    public void upgradeSystemContract(
        byte[] contractJar, byte[] binderJar, byte[] rpc, BlockchainAddress contractAddress) {
      upgradeSystemContract(contractJar, binderJar, new byte[0], rpc, contractAddress);
    }

    @Override
    public void upgradeSystemContract(
        byte[] contractJar,
        byte[] binderJar,
        byte[] abi,
        byte[] rpc,
        BlockchainAddress contractAddress) {
      updateTracker.accept(
          new UpdateContractEvent(contractJar, binderJar, abi, rpc, contractAddress));
    }
  }
}
