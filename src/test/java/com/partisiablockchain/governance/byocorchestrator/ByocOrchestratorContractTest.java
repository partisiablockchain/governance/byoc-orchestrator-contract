package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.ContractEvent;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.ContractEventDeploy;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.immutable.FixedList;
import java.util.List;
import java.util.Map;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

final class ByocOrchestratorContractTest {

  private final SysContractSerialization<ByocOrchestratorContractState> serialization =
      new SysContractSerialization<>(
          ByocOrchestratorContractInvoker.class, ByocOrchestratorContractState.class);

  private static final BlockchainAddress votingContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000001");
  private static final BlockchainAddress largeOracleContract =
      BlockchainAddress.fromString("040000000000000000000000000000000000000002");
  private static final ExternalBlockchainAddressRpc externalAddress =
      new ExternalBlockchainAddressRpc(new byte[20]);

  private static final PriceOracleConfiguration priceOracleConfiguration =
      new PriceOracleConfiguration("priceOracleReference", 18, 8, "Ethereum");
  private static final BridgeParametersRpc bridgeParams =
      new BridgeParametersRpc(Unsigned256.TEN, Unsigned256.ONE, Unsigned256.TEN);

  private static final ContractBundleRpc incomingContractBundle =
      new ContractBundleRpc(ContractType.INCOMING, new byte[] {1}, new byte[] {2}, new byte[] {3});
  private static final ContractBundleRpc outgoingContractBundle =
      new ContractBundleRpc(ContractType.OUTGOING, new byte[] {4}, new byte[] {5}, new byte[] {6});
  private static final ContractBundleRpc byocTokenContractBundle =
      new ContractBundleRpc(
          ContractType.BYOC_TOKEN, new byte[] {7}, new byte[] {8}, new byte[] {9});
  private static final ContractBundleRpc priceOracleContractBundle =
      new ContractBundleRpc(
          ContractType.PRICE_ORACLE, new byte[] {10}, new byte[] {11}, new byte[] {12});

  private static final String SYM_1 = "SYM1";
  private static final String SYM_2 = "SYM2";
  private static final List<String> initialSymbols = List.of(SYM_1, SYM_2);
  private static final String CHAIN_1 = "CHAIN1";
  private static final String CHAIN_2 = "CHAIN2";
  private static final String CHAIN_3 = "CHAIN3";
  private static final Bridge SYM_1_BRIDGE =
      new Bridge(
          CHAIN_1,
          BlockchainAddress.fromString("040000000000000000000000000000000000000010"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000011"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000012"),
          null,
          FixedList.create());
  private static final Bridge SYM_1_BRIDGE_WITH_TOKEN_BRIDGE =
      new Bridge(
          CHAIN_1,
          BlockchainAddress.fromString("040000000000000000000000000000000000000010"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000011"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000012"),
          null,
          FixedList.create(
              List.of(
                  new TokenBridge(
                      CHAIN_2,
                      BlockchainAddress.fromString("040000000000000000000000000000000000000013"),
                      BlockchainAddress.fromString(
                          "040000000000000000000000000000000000000014")))));

  private static final Bridge SYM_2_BRIDGE =
      new Bridge(
          CHAIN_2,
          BlockchainAddress.fromString("040000000000000000000000000000000000000020"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000021"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000022"),
          BlockchainAddress.fromString("040000000000000000000000000000000000000023"),
          FixedList.create());

  private SysContractContextTest context;
  private ByocOrchestratorContractState initialState;

  /**
   * Write a Bridge to the stream.
   *
   * @param bridge the bridge to serialize
   * @param stream the stream to write to
   */
  static void writeBridge(Bridge bridge, SafeDataOutputStream stream) {
    stream.writeString(bridge.chain());
    bridge.incoming().write(stream);
    bridge.outgoing().write(stream);
    bridge.byocToken().write(stream);
    stream.writeOptional(BlockchainAddress::write, bridge.priceOracle());
    stream.writeInt(bridge.tokenBridges().size());
    for (TokenBridge additionalBridge : bridge.tokenBridges()) {
      writeTokenBridge(additionalBridge, stream);
    }
  }

  /**
   * Write a TokenBridge to the stream.
   *
   * @param bridge the token bridge to serialize
   * @param stream the stream to write to
   */
  static void writeTokenBridge(TokenBridge bridge, SafeDataOutputStream stream) {
    stream.writeString(bridge.chain());
    bridge.incoming().write(stream);
    bridge.outgoing().write(stream);
  }

  @BeforeEach
  public void setUp() {
    context = new SysContractContextTest(0, 100, votingContract);
    initialState =
        new ByocOrchestratorContractState(
            votingContract,
            largeOracleContract,
            AvlTree.create(Map.of(SYM_1, SYM_1_BRIDGE, SYM_2, SYM_2_BRIDGE)),
            AvlTree.create(
                Map.of(
                    ContractType.INCOMING,
                    incomingContractBundle.convert(),
                    ContractType.OUTGOING,
                    outgoingContractBundle.convert(),
                    ContractType.BYOC_TOKEN,
                    byocTokenContractBundle.convert(),
                    ContractType.PRICE_ORACLE,
                    priceOracleContractBundle.convert())));
  }

  @Test
  @SuppressWarnings("EnumOrdinal")
  void create() {
    SysContractContextTestDecorator decorated = new SysContractContextTestDecorator(context);
    ByocOrchestratorContractState state =
        serialization.create(
            decorated,
            rpc -> {
              votingContract.write(rpc);
              largeOracleContract.write(rpc);

              rpc.writeInt(4); // size of List<ContractBundleRpc>
              rpc.writeByte(incomingContractBundle.type().ordinal());
              rpc.writeDynamicBytes(incomingContractBundle.contract());
              rpc.writeDynamicBytes(incomingContractBundle.binder());
              rpc.writeDynamicBytes(incomingContractBundle.abi());

              rpc.writeByte(outgoingContractBundle.type().ordinal());
              rpc.writeDynamicBytes(outgoingContractBundle.contract());
              rpc.writeDynamicBytes(outgoingContractBundle.binder());
              rpc.writeDynamicBytes(outgoingContractBundle.abi());

              rpc.writeByte(byocTokenContractBundle.type().ordinal());
              rpc.writeDynamicBytes(byocTokenContractBundle.contract());
              rpc.writeDynamicBytes(byocTokenContractBundle.binder());
              rpc.writeDynamicBytes(byocTokenContractBundle.abi());

              rpc.writeByte(priceOracleContractBundle.type().ordinal());
              rpc.writeDynamicBytes(priceOracleContractBundle.contract());
              rpc.writeDynamicBytes(priceOracleContractBundle.binder());
              rpc.writeDynamicBytes(priceOracleContractBundle.abi());

              rpc.writeInt(2); // number of coins
              rpc.writeString(initialSymbols.get(0));
              writeBridge(SYM_1_BRIDGE_WITH_TOKEN_BRIDGE, rpc);

              rpc.writeString(initialSymbols.get(1));
              writeBridge(SYM_2_BRIDGE, rpc);
            });
    Assertions.assertThat(state.getVotingContract()).isEqualTo(votingContract);
    Bridge bridge1 = state.getBridge(SYM_1);
    Assertions.assertThat(bridge1).isNotNull();
    StateSerializableEquality.assertStatesEqual(bridge1, SYM_1_BRIDGE_WITH_TOKEN_BRIDGE);
    Bridge bridge2 = state.getBridge(SYM_2);
    Assertions.assertThat(bridge2).isNotNull();
    StateSerializableEquality.assertStatesEqual(bridge2, SYM_2_BRIDGE);

    // 9 updates because there's 1 BYOC bridge with 3 contracts, with a token bridge containing 2
    // contracts, and another BYOC bridge with 4 contracts.
    List<UpdateContractEvent> updates = decorated.getUpdates();
    Assertions.assertThat(updates).hasSize(9);

    Assertions.assertThat(updates.get(0).contractAddress())
        .isEqualTo(SYM_1_BRIDGE_WITH_TOKEN_BRIDGE.incoming());
    Assertions.assertThat(updates.get(1).contractAddress()).isEqualTo(SYM_2_BRIDGE.incoming());
    Assertions.assertThat(updates.get(2).contractAddress())
        .isEqualTo(SYM_1_BRIDGE_WITH_TOKEN_BRIDGE.tokenBridges().get(0).incoming());

    Assertions.assertThat(updates.get(3).contractAddress())
        .isEqualTo(SYM_1_BRIDGE_WITH_TOKEN_BRIDGE.outgoing());
    Assertions.assertThat(updates.get(4).contractAddress()).isEqualTo(SYM_2_BRIDGE.outgoing());
    Assertions.assertThat(updates.get(5).contractAddress())
        .isEqualTo(SYM_1_BRIDGE_WITH_TOKEN_BRIDGE.tokenBridges().get(0).outgoing());

    Assertions.assertThat(updates.get(6).contractAddress())
        .isEqualTo(SYM_1_BRIDGE_WITH_TOKEN_BRIDGE.byocToken());
    Assertions.assertThat(updates.get(7).contractAddress()).isEqualTo(SYM_2_BRIDGE.byocToken());

    Assertions.assertThat(updates.get(8).contractAddress()).isEqualTo(SYM_2_BRIDGE.priceOracle());
  }

  @Test
  void addBridge() {
    String symbol = "SYM3";
    ByocOrchestratorContractState state = addByoc(symbol, "Ethereum");

    Assertions.assertThat(state.getBridge(symbol)).isNull();

    byte[] setCoinRpc = context.getUpdateGlobalAccountPluginState().getRpc();
    byte[] expectedSetCoinRpc =
        SafeDataOutputStream.serialize(
            rpc -> {
              rpc.writeByte(AccountPluginRpcHelper.ACCOUNT_PLUGIN_SET_COIN);
              rpc.writeString(symbol);
              rpc.writeLong(1);
              rpc.writeLong(2);
            });
    Assertions.assertThat(setCoinRpc).isEqualTo(expectedSetCoinRpc);

    byte[] callbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            ByocOrchestratorContract.Callbacks.addByocCallback(
                symbol, "chain", bridgeParams, externalAddress, priceOracleConfiguration));
    Assertions.assertThat(callbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void onlyVotingContractCanAddBridge() {
    BlockchainAddress notVotingContract =
        BlockchainAddress.fromString("040000000000000000000000000000000000000123");
    context = new SysContractContextTest(0, 100, notVotingContract);

    Assertions.assertThatThrownBy(() -> addByoc("SYM3", "Ethereum"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the voting contract can add a BYOC bridge");
  }

  @Test
  void cannotAddExistingSymbol() {
    Assertions.assertThatThrownBy(() -> addByoc(initialSymbols.get(0), "Ethereum"))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Symbol already exists");
  }

  @ParameterizedTest
  @ValueSource(booleans = {true, false})
  void addBridgeCallback(boolean withPriceOracle) {
    final Hash txHash = Hash.create(s -> s.writeLong(0));
    final PriceOracleConfiguration priceOracleConfiguration;
    final BlockchainAddress expectedPriceOracleAddress;
    if (withPriceOracle) {
      priceOracleConfiguration = ByocOrchestratorContractTest.priceOracleConfiguration;
      expectedPriceOracleAddress =
          BlockchainAddress.fromHash(
              BlockchainAddress.Type.CONTRACT_GOV,
              Hash.create(
                  s -> {
                    txHash.write(s);
                    s.writeString("price-oracle");
                  }));
    } else {
      priceOracleConfiguration = null;
      expectedPriceOracleAddress = null;
    }

    CallbackContext callbackContext = createCallbackContext(true);
    ByocOrchestratorContractState state =
        serialization.callback(
            context,
            callbackContext,
            initialState,
            ByocOrchestratorContract.Callbacks.addByocCallback(
                "SYM3", "chain", bridgeParams, externalAddress, priceOracleConfiguration));

    Bridge bridge = state.getBridge("SYM3");
    Assertions.assertThat(bridge).isNotNull();

    BlockchainAddress expectedIncomingAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV,
            Hash.create(
                s -> {
                  txHash.write(s);
                  s.writeString("deposit");
                }));
    BlockchainAddress expectedOutgoingAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_GOV,
            Hash.create(
                s -> {
                  txHash.write(s);
                  s.writeString("withdraw");
                }));
    BlockchainAddress expectedByocTokenAddress =
        BlockchainAddress.fromHash(
            BlockchainAddress.Type.CONTRACT_SYSTEM,
            Hash.create(
                s -> {
                  txHash.write(s);
                  s.writeString("byoc-token");
                }));
    StateSerializableEquality.assertStatesEqual(
        bridge,
        new Bridge(
            "chain",
            expectedIncomingAddress,
            expectedOutgoingAddress,
            expectedByocTokenAddress,
            expectedPriceOracleAddress,
            FixedList.create()));

    ContractEventDeploy incoming = (ContractEventDeploy) context.getInteractions().get(0);
    Assertions.assertThat(incoming.deploy.contract).isEqualTo(expectedIncomingAddress);
    Assertions.assertThat(incoming.deploy.abi).isEqualTo(incomingContractBundle.abi());
    Assertions.assertThat(incoming.deploy.contractJar).isEqualTo(incomingContractBundle.contract());
    Assertions.assertThat(incoming.deploy.binderJar).isEqualTo(incomingContractBundle.binder());
    Assertions.assertThat(incoming.allocatedCost).isEqualTo(0L);
    byte[] expectedIncomingDeployRpc =
        SafeDataOutputStream.serialize(
            rpc -> {
              largeOracleContract.write(rpc);
              rpc.writeString("SYM3");
              bridgeParams.depositMaximum().write(rpc);
            });
    byte[] actualIncomingDeployRpc = SafeDataOutputStream.serialize(incoming.deploy.rpc);
    Assertions.assertThat(actualIncomingDeployRpc).isEqualTo(expectedIncomingDeployRpc);

    ContractEventDeploy outgoing = (ContractEventDeploy) context.getInteractions().get(1);
    Assertions.assertThat(outgoing.deploy.contract).isEqualTo(expectedOutgoingAddress);
    Assertions.assertThat(outgoing.deploy.abi).isEqualTo(outgoingContractBundle.abi());

    Assertions.assertThat(outgoing.deploy.contractJar).isEqualTo(outgoingContractBundle.contract());
    Assertions.assertThat(outgoing.deploy.binderJar).isEqualTo(outgoingContractBundle.binder());
    Assertions.assertThat(outgoing.allocatedCost).isEqualTo(0L);
    byte[] expectedOutgoingDeployRpc =
        SafeDataOutputStream.serialize(
            rpc -> {
              largeOracleContract.write(rpc);
              externalAddress.write(rpc);
              rpc.writeString("SYM3");
              rpc.writeInt(0);
              rpc.writeInt(0);
              bridgeParams.withdrawMinimum().write(rpc);
              bridgeParams.withdrawMaximum().write(rpc);
            });
    byte[] actualOutgoingDeployRpc = SafeDataOutputStream.serialize(outgoing.deploy.rpc);
    Assertions.assertThat(actualOutgoingDeployRpc).isEqualTo(expectedOutgoingDeployRpc);

    ContractEventDeploy byocToken = (ContractEventDeploy) context.getInteractions().get(2);
    Assertions.assertThat(byocToken.deploy.contract).isEqualTo(expectedByocTokenAddress);
    Assertions.assertThat(byocToken.deploy.abi).isEqualTo(byocTokenContractBundle.abi());
    Assertions.assertThat(byocToken.allocatedCost).isNull();

    Assertions.assertThat(byocToken.deploy.contractJar)
        .isEqualTo(byocTokenContractBundle.contract());
    Assertions.assertThat(byocToken.deploy.binderJar).isEqualTo(byocTokenContractBundle.binder());
    byte[] expectedByocTokenDeployRpc =
        SafeDataOutputStream.serialize(rpc -> rpc.writeString("SYM3"));
    byte[] actualByocTokenDeployRpc = SafeDataOutputStream.serialize(byocToken.deploy.rpc);
    Assertions.assertThat(actualByocTokenDeployRpc).isEqualTo(expectedByocTokenDeployRpc);

    if (withPriceOracle) {
      ContractEventDeploy priceOracleDeploy =
          (ContractEventDeploy) context.getInteractions().get(3);
      Assertions.assertThat(priceOracleDeploy.deploy.contract)
          .isEqualTo(expectedPriceOracleAddress);
      Assertions.assertThat(priceOracleDeploy.deploy.abi)
          .isEqualTo(priceOracleContractBundle.abi());

      Assertions.assertThat(priceOracleDeploy.deploy.contractJar)
          .isEqualTo(priceOracleContractBundle.contract());
      Assertions.assertThat(priceOracleDeploy.deploy.binderJar)
          .isEqualTo(priceOracleContractBundle.binder());
      Assertions.assertThat(priceOracleDeploy.allocatedCost).isEqualTo(0L);
      byte[] expectedPriceOracleDeployRpc =
          SafeDataOutputStream.serialize(
              rpc -> {
                rpc.writeString("SYM3");
                largeOracleContract.write(rpc);
                priceOracleConfiguration.write(rpc);
              });
      byte[] actualPriceOracleDeployRpc =
          SafeDataOutputStream.serialize(priceOracleDeploy.deploy.rpc);
      Assertions.assertThat(actualPriceOracleDeployRpc).isEqualTo(expectedPriceOracleDeployRpc);
    }
  }

  @Test
  void callbackIdempotentIfNotSuccessful() {
    CallbackContext callbackContext = createCallbackContext(false);
    ByocOrchestratorContractState state =
        serialization.callback(
            context,
            callbackContext,
            initialState,
            ByocOrchestratorContract.Callbacks.addByocCallback(
                "a", "b", bridgeParams, externalAddress, null));
    Assertions.assertThat(state.getBridge("a")).isNull();
    Assertions.assertThat(context.getInteractions()).isEmpty();
  }

  @Test
  @SuppressWarnings("EnumOrdinal")
  void onlyVotingCanUpdateContractBundle() {
    BlockchainAddress notVotingContract =
        BlockchainAddress.fromString("040000000000000000000000000000000000000123");
    context = new SysContractContextTest(0, 100, notVotingContract);

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initialState,
                    rpc -> {
                      rpc.writeByte(ByocOrchestratorContract.Invocations.UPDATE_CONTRACT_BUNDLE);
                      // only the type needs to be valid
                      rpc.writeByte(ContractType.INCOMING.ordinal());
                      rpc.writeDynamicBytes(new byte[0]);
                      rpc.writeDynamicBytes(new byte[0]);
                      rpc.writeDynamicBytes(new byte[0]);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the voting contract can update a contract");
  }

  @Test
  @SuppressWarnings("EnumOrdinal")
  void updateContractBundle() {

    ContractBundleRpc newContract =
        new ContractBundleRpc(
            ContractType.INCOMING, new byte[] {42}, new byte[] {43}, new byte[] {44});

    SysContractContextTestDecorator decorated = new SysContractContextTestDecorator(context);
    ByocOrchestratorContractState state =
        addTokenBridgeToExistingByoc(initialState, SYM_1, CHAIN_2, bridgeParams, externalAddress);
    state =
        serialization.invoke(
            decorated,
            state,
            rpc -> {
              rpc.writeByte(ByocOrchestratorContract.Invocations.UPDATE_CONTRACT_BUNDLE);

              rpc.writeByte(newContract.type().ordinal());
              rpc.writeDynamicBytes(newContract.contract());
              rpc.writeDynamicBytes(newContract.binder());
              rpc.writeDynamicBytes(newContract.abi());
            });

    ContractBundle newBundle = newContract.convert();
    StateSerializableEquality.assertStatesEqual(
        state.getContractBundle(newContract.type()), newBundle);

    // the other contract bundle is left untouched
    StateSerializableEquality.assertStatesEqual(
        state.getContractBundle(ContractType.OUTGOING), outgoingContractBundle.convert());

    List<UpdateContractEvent> updates = decorated.getUpdates();
    Assertions.assertThat(updates).hasSize(3);

    // check that the right addresses were updated
    Assertions.assertThat(updates.get(0).contractAddress())
        .isEqualTo(state.getBridge(SYM_1).incoming());
    Assertions.assertThat(updates.get(1).contractAddress())
        .isEqualTo(state.getBridge(SYM_2).incoming());
    // Third update is on a token bridge for the SYM_1 chain
    Assertions.assertThat(updates.get(2).contractAddress())
        .isEqualTo(state.getBridge(SYM_1).tokenBridges().get(0).incoming());

    // check that the updates where done with the right data
    Assertions.assertThat(updates.get(0).abi()).isEqualTo(newContract.abi());
    Assertions.assertThat(updates.get(1).abi()).isEqualTo(newContract.abi());
    Assertions.assertThat(updates.get(2).abi()).isEqualTo(newContract.abi());

    Assertions.assertThat(updates.get(0).contractJar()).isEqualTo(newContract.contract());
    Assertions.assertThat(updates.get(1).contractJar()).isEqualTo(newContract.contract());
    Assertions.assertThat(updates.get(2).contractJar()).isEqualTo(newContract.contract());

    Assertions.assertThat(updates.get(0).binderJar()).isEqualTo(newContract.binder());
    Assertions.assertThat(updates.get(1).binderJar()).isEqualTo(newContract.binder());
    Assertions.assertThat(updates.get(2).binderJar()).isEqualTo(newContract.binder());
  }

  @Test
  void migrate() {
    ByocOrchestratorContract contract = new ByocOrchestratorContract();
    ByocOrchestratorContractState initialStateWithTokenBridge =
        contract.addTokenBridge(
            context, initialState, SYM_1, CHAIN_2, bridgeParams, externalAddress);
    StateAccessor current = StateAccessor.create(initialStateWithTokenBridge);
    ByocOrchestratorContractState upgraded = contract.upgrade(current);

    Assertions.assertThat(upgraded.getVotingContract())
        .isEqualTo(initialStateWithTokenBridge.getVotingContract());
    Assertions.assertThat(upgraded.getLargeOracleContract())
        .isEqualTo(initialStateWithTokenBridge.getLargeOracleContract());
    for (String symbol : initialSymbols) {
      StateSerializableEquality.assertStatesEqual(
          upgraded.getBridge(symbol), initialStateWithTokenBridge.getBridge(symbol));
    }

    Assertions.assertThat(upgraded.getContractBundle(ContractType.INCOMING))
        .isEqualTo(initialStateWithTokenBridge.getContractBundle(ContractType.INCOMING));
    Assertions.assertThat(upgraded.getContractBundle(ContractType.OUTGOING))
        .isEqualTo(initialStateWithTokenBridge.getContractBundle(ContractType.OUTGOING));
  }

  /** Only the voting contract can add additional token bridges. */
  @Test
  void onlyTheVotingContractCanAddAdditionalBridges() {
    context =
        new SysContractContextTest(
            1, 1, BlockchainAddress.fromString("040000000000000000000000000000000000000003"));

    Assertions.assertThatThrownBy(
            () ->
                addTokenBridgeToExistingByoc(
                    initialState, "SYM3", "Ethereum", bridgeParams, externalAddress))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only the voting contract can add a token bridge to an existing BYOC");
  }

  /** It is only possible to add token bridges for BYOC that exist on PBC. */
  @Test
  void cannotAddTokenBridgeToExistingByocIfByocDoesNotExist() {
    String coinSymbol = "newByoc";
    Assertions.assertThatThrownBy(
            () ->
                addTokenBridgeToExistingByoc(
                    initialState, coinSymbol, CHAIN_1, bridgeParams, externalAddress))
        .hasMessageContaining("%s does not exist on PBC".formatted(coinSymbol));
  }

  /**
   * It is not possible to add a token bridge for a coin and chain, if a BYOC bridge already exists
   * for this coin and chain.
   */
  @Test
  void cannotAddBridgeToExistingCoinIfTokenBridgeAlreadyExists() {
    Assertions.assertThatThrownBy(
            () ->
                addTokenBridgeToExistingByoc(
                    initialState, SYM_1, CHAIN_1, bridgeParams, externalAddress))
        .hasMessageContaining(
            ("Can not deploy a token bridge for coin %s and chain %s when there already exists an "
                    + "identical BYOC bridge")
                .formatted(SYM_1, CHAIN_1));
  }

  /**
   * Adding a new token bridge to an existing BYOC deploys the deposit and withdrawal contracts, and
   * adds the token bridge to the BYOC bridge.
   */
  @Test
  void addTokenBridgeToExistingByocAddsTheTokenBrigeToTheMainTokenBridge() {
    ByocOrchestratorContractState state =
        addTokenBridgeToExistingByoc(initialState, SYM_2, CHAIN_3, bridgeParams, externalAddress);
    Assertions.assertThat(state.getBridge(SYM_2).tokenBridges().size()).isEqualTo(1);
    Assertions.assertThat(state.getTokenBridge(SYM_2, CHAIN_2)).isNull();
    TokenBridge tokenBridge = state.getTokenBridge(SYM_2, CHAIN_3);
    Assertions.assertThat(tokenBridge).isNotNull();
    Assertions.assertThat(tokenBridge.chain()).isEqualTo(CHAIN_3);

    // Ensure the deposit, withdrawal and token contracts are deployed
    List<ContractEvent> contractEventDeploys = context.getInteractions();
    Assertions.assertThat(contractEventDeploys.size()).isEqualTo(2);
    Assertions.assertThat(((ContractEventDeploy) contractEventDeploys.get(0)).deploy.contractJar)
        .isEqualTo(incomingContractBundle.contract());
    Assertions.assertThat(((ContractEventDeploy) contractEventDeploys.get(1)).deploy.contractJar)
        .isEqualTo(outgoingContractBundle.contract());

    // Ensure contracts are stored in token bridge
    Assertions.assertThat(tokenBridge.incoming()).isNotNull();
    Assertions.assertThat(tokenBridge.outgoing()).isNotNull();
  }

  /** It is not possible to have two identical token bridges. */
  @Test
  void cannotAddTokenBridgeIfAlreadyExists() {
    ByocOrchestratorContractState state =
        addTokenBridgeToExistingByoc(initialState, SYM_2, CHAIN_3, bridgeParams, externalAddress);
    Assertions.assertThatThrownBy(
            () ->
                addTokenBridgeToExistingByoc(state, SYM_2, CHAIN_3, bridgeParams, externalAddress))
        .hasMessageContaining(
            "Token bridge for coin %s and chain %s already exists".formatted(SYM_2, CHAIN_3));
  }

  /** The old contract state can be migrated to the newest version. */
  @Test
  void migrateOldState() {
    ByocOchestratorContractStateOld oldState =
        new ByocOchestratorContractStateOld(
            votingContract,
            largeOracleContract,
            AvlTree.create(
                Map.of(
                    SYM_1,
                    new BridgeOld(
                        SYM_1_BRIDGE.chain(),
                        SYM_1_BRIDGE.incoming(),
                        SYM_1_BRIDGE.outgoing(),
                        SYM_1_BRIDGE.byocToken(),
                        SYM_1_BRIDGE.priceOracle()),
                    SYM_2,
                    new BridgeOld(
                        SYM_2_BRIDGE.chain(),
                        SYM_2_BRIDGE.incoming(),
                        SYM_2_BRIDGE.outgoing(),
                        SYM_2_BRIDGE.byocToken(),
                        SYM_2_BRIDGE.priceOracle()))),
            AvlTree.create(
                Map.of(
                    ContractType.INCOMING,
                    incomingContractBundle.convert(),
                    ContractType.OUTGOING,
                    outgoingContractBundle.convert(),
                    ContractType.BYOC_TOKEN,
                    byocTokenContractBundle.convert(),
                    ContractType.PRICE_ORACLE,
                    priceOracleContractBundle.convert())));
    StateAccessor oldStateAccessor = StateAccessor.create(oldState);

    ByocOrchestratorContractState newState =
        ByocOrchestratorContractState.migrateState(oldStateAccessor);
    Assertions.assertThat(newState.getLargeOracleContract()).isEqualTo(largeOracleContract);
    Assertions.assertThat(newState.getVotingContract()).isEqualTo(votingContract);
    Assertions.assertThat(newState.getContractBundle(ContractType.INCOMING).contract().getData())
        .isEqualTo(incomingContractBundle.contract());
    Assertions.assertThat(newState.getContractBundle(ContractType.OUTGOING).contract().getData())
        .isEqualTo(outgoingContractBundle.contract());
    Assertions.assertThat(newState.getContractBundle(ContractType.BYOC_TOKEN).contract().getData())
        .isEqualTo(byocTokenContractBundle.contract());
    Assertions.assertThat(
            newState.getContractBundle(ContractType.PRICE_ORACLE).contract().getData())
        .isEqualTo(priceOracleContractBundle.contract());
    Assertions.assertThat(newState.getTokenBridges(SYM_1)).isEmpty();
    Assertions.assertThat(newState.getTokenBridges(SYM_2)).isEmpty();
  }

  private ByocOrchestratorContractState addByoc(String symbol, String chainId) {
    return serialization.invoke(
        context,
        initialState,
        rpc -> {
          rpc.writeByte(ByocOrchestratorContract.Invocations.ADD_BYOC);
          rpc.writeString(symbol);
          rpc.writeString("chain");
          rpc.writeLong(1);
          rpc.writeLong(2);
          bridgeParams.write(rpc);
          externalAddress.write(rpc);
          rpc.writeOptional(
              PriceOracleConfiguration::write,
              new PriceOracleConfiguration("priceOracleReference", 18, 8, chainId));
        });
  }

  private ByocOrchestratorContractState addTokenBridgeToExistingByoc(
      ByocOrchestratorContractState state,
      String symbol,
      String chainId,
      BridgeParametersRpc bridge,
      ExternalBlockchainAddressRpc externalAddress) {
    return serialization.invoke(
        context,
        state,
        rpc -> {
          rpc.writeByte(ByocOrchestratorContract.Invocations.ADD_TOKEN_BRIDGE);
          rpc.writeString(symbol);
          rpc.writeString(chainId);
          bridge.write(rpc);
          externalAddress.write(rpc);
        });
  }

  private CallbackContext createCallbackContext(boolean result) {
    Hash tx = Hash.create(s -> s.writeInt(123));
    CallbackContext.ExecutionResult executionResult =
        CallbackContext.createResult(tx, result, SafeDataInputStream.createFromBytes(new byte[0]));
    return CallbackContext.create(FixedList.create(List.of(executionResult)));
  }

  /** The old BYOC orchestrator contract state, with the old version of a bridge. */
  @Immutable
  private record ByocOchestratorContractStateOld(
      BlockchainAddress votingContract,
      BlockchainAddress largeOracleContract,
      AvlTree<String, BridgeOld> byocBridges,
      AvlTree<ContractType, ContractBundle> contractBundles)
      implements StateSerializable {}

  /** The old bridge configuration, without token bridges. */
  @Immutable
  private record BridgeOld(
      String chain,
      BlockchainAddress incoming,
      BlockchainAddress outgoing,
      BlockchainAddress byocToken,
      BlockchainAddress priceOracle)
      implements StateSerializable {}
}
