package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.serialization.LargeByteArray;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * Holds information required to deploy a contract.
 *
 * @param contract the contract JAR
 * @param binder the binder JAR
 * @param abi the ABI for the contract
 */
@Immutable
record ContractBundle(LargeByteArray contract, LargeByteArray binder, LargeByteArray abi)
    implements StateSerializable {

  static ContractBundle createFromStateAccessor(StateAccessor accessor) {
    return new ContractBundle(
        accessor.get("contract").cast(LargeByteArray.class),
        accessor.get("binder").cast(LargeByteArray.class),
        accessor.get("abi").cast(LargeByteArray.class));
  }
}
