package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.reflect.RpcType;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/**
 * An RPC-friendly version of a BYOC bridge.
 *
 * @param chain the external chain the bridge is associated with
 * @param incoming the deposit contract on PBC
 * @param outgoing the withdrawal contract on PBC
 * @param byocToken the MPC20 contract on PBC
 * @param priceOracle the price oracle contract on PBC
 * @param additionalBridges the token bridges associated with this bridge
 */
record BridgeRpc(
    String chain,
    BlockchainAddress incoming,
    BlockchainAddress outgoing,
    BlockchainAddress byocToken,
    @RpcType(nullable = true) BlockchainAddress priceOracle,
    List<TokenBridge> additionalBridges) {

  Bridge convert() {
    return new Bridge(
        chain(),
        incoming(),
        outgoing(),
        byocToken(),
        priceOracle(),
        FixedList.create(additionalBridges()));
  }
}
