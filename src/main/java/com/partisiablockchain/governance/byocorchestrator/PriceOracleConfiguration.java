package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.DataStreamSerializable;
import com.secata.stream.SafeDataOutputStream;

/**
 * Configuration for deploying a price oracle contract.
 *
 * @param priceOracleReferenceContract address of the reference contract
 * @param pbcDecimals number of decimals used to denote the BYOC balance on PBC
 * @param usdDecimals number of decimals used to denote the price in USD on Chainlink
 * @param chainId identifier for the EVM chain on which the Chainlink reference contract resides
 */
public record PriceOracleConfiguration(
    String priceOracleReferenceContract, int pbcDecimals, int usdDecimals, String chainId)
    implements DataStreamSerializable {

  @Override
  public void write(SafeDataOutputStream stream) {
    stream.writeString(priceOracleReferenceContract());
    stream.writeInt(pbcDecimals());
    stream.writeInt(usdDecimals());
    stream.writeString(chainId());
  }
}
