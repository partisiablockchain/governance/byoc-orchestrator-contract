package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.secata.stream.SafeDataOutputStream;

/** Helper class containing methods for invoking the account plugin. */
final class AccountPluginRpcHelper {

  private AccountPluginRpcHelper() {}

  static final byte ACCOUNT_PLUGIN_SET_COIN = 2;

  /**
   * Creates the RPC payload for the SET_COIN action on the global account plugin.
   *
   * @param coin the coin to set.
   * @return the RPC payload
   */
  static byte[] setCoin(CoinRpc coin) {
    return SafeDataOutputStream.serialize(
        rpc -> {
          rpc.writeByte(ACCOUNT_PLUGIN_SET_COIN);
          rpc.writeString(coin.symbol());
          rpc.writeLong(coin.conversionNumerator());
          rpc.writeLong(coin.conversionDenominator());
        });
  }
}
