package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/**
 * A token bridge for BYOC.
 *
 * <p>A token bridge is used to deposit and withdraw existing BYOC to and from external chains where
 * it is minted as an ERC20. A token bridge is thus similar to a BYOC bridge, but whereas a BYOC
 * bridge transfers new BYOC, that exists on an external chain, onto PBC, token bridges are used to
 * bridge BYOC that already exists on PBC to additional external chains where it is minted. As an
 * example, if one wants to bridge Eth from Ethereum onto PBC, a BYOC bridge would be used. If one
 * then wants to bridge Eth from PBC to Polygon, a token bridge would be used, resulting in a
 * (PBC-wrapped) version of Eth being bridged to Polygon.
 *
 * @param chain the external chain the bridge is associated with
 * @param incoming the deposit contract on PBC
 * @param outgoing the withdrawal contract on PBC
 */
@Immutable
public record TokenBridge(String chain, BlockchainAddress incoming, BlockchainAddress outgoing)
    implements StateSerializable {

  static TokenBridge createFromStateAccessor(StateAccessor tokenBridgeAccessor) {
    return new TokenBridge(
        tokenBridgeAccessor.get("chain").stringValue(),
        tokenBridgeAccessor.get("incoming").blockchainAddressValue(),
        tokenBridgeAccessor.get("outgoing").blockchainAddressValue());
  }
}
