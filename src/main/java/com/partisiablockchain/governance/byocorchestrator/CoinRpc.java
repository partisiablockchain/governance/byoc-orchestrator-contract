package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 * This record defines a coin that is to be bridged. A coin is a symbol and a conversion rate
 * provided as <code>numerator / denominator</code>. The conversion rate tells us how much gas each
 * coin corresponds to.
 *
 * @param symbol the symbol of the coin
 * @param chain a descriptor for the chain that this bridged coin resides on
 * @param conversionNumerator the numerator of the conversion rate for this coin
 * @param conversionDenominator the denominator of the conversion rate for this coin
 */
record CoinRpc(String symbol, String chain, long conversionNumerator, long conversionDenominator) {}
