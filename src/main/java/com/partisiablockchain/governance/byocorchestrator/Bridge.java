package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines a BYOC bridge for external coins on the PBC side. A BYOC bridge is used to deposit and
 * withdraw external coins (BYOC), e.g. Eth, between an external chain, e.g. Ethereum, and PBC. The
 * bridge contains information on which chain the bridge connects to, as well as the addresses of
 * the PBC contracts that are responsible for deposits, withdrawals, the token contract used to
 * transfer the BYOC on PBC, and an optional price oracle contract.
 *
 * @param chain a descriptor for the chain this bridge connects to
 * @param incoming the PBC address of the deposit contract
 * @param outgoing the PBC address of the withdrawal contract
 * @param byocToken the PBC address of the byoc token contract
 * @param priceOracle the PBC address of the price oracle if one is deployed otherwise null
 */
@Immutable
record Bridge(
    String chain,
    BlockchainAddress incoming,
    BlockchainAddress outgoing,
    BlockchainAddress byocToken,
    BlockchainAddress priceOracle,
    FixedList<TokenBridge> tokenBridges)
    implements StateSerializable {

  static Bridge createFromStateAccessor(StateAccessor accessor) {
    List<TokenBridge> tokenBridges = new ArrayList<>();
    if (accessor.hasField("tokenBridges")) {
      for (StateAccessor tokenBridgeAccessor : accessor.get("tokenBridges").getListElements()) {
        tokenBridges.add(TokenBridge.createFromStateAccessor(tokenBridgeAccessor));
      }
    }
    return new Bridge(
        accessor.get("chain").stringValue(),
        accessor.get("incoming").blockchainAddressValue(),
        accessor.get("outgoing").blockchainAddressValue(),
        accessor.get("byocToken").blockchainAddressValue(),
        accessor.get("priceOracle").blockchainAddressValue(),
        FixedList.create(tokenBridges));
  }
}
