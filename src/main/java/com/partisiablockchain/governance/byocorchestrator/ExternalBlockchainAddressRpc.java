package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.contract.reflect.RpcType;
import com.secata.stream.SafeDataOutputStream;

/**
 * Represents an external blockchain address. The size of an external address is for the moment
 * fixed at 20 bytes, which is the size of an address on Ethereum.
 *
 * @param bytes an address in byte form
 */
@SuppressWarnings("ArrayRecordComponent")
record ExternalBlockchainAddressRpc(@RpcType(size = 20) byte[] bytes) {

  void write(SafeDataOutputStream stream) {
    stream.write(bytes());
  }
}
