package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.serialization.LargeByteArray;

/**
 * The RPC definition for a {@link ContractBundle}. The RPC for a ContractBundle also carries
 * information about what contract it is, i.e., whether it is a byoc-incoming or byoc-outgoing
 * contract.
 *
 * @param type contract type
 * @param contract the contract JAR
 * @param binder the binder JAR
 * @param abi the ABI
 */
@SuppressWarnings("ArrayRecordComponent")
record ContractBundleRpc(ContractType type, byte[] contract, byte[] binder, byte[] abi) {

  /**
   * Convert to a {@link ContractBundle}.
   *
   * @return a {@link ContractBundle}
   */
  ContractBundle convert() {
    return new ContractBundle(
        new LargeByteArray(contract()), new LargeByteArray(binder()), new LargeByteArray(abi()));
  }
}
