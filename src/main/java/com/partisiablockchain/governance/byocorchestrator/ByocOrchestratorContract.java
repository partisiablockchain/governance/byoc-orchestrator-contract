package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.FormatMethod;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.GlobalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.immutable.FixedList;
import java.util.List;

/**
 * Smart contract that manages BYOC bridges.
 *
 * <p>This smart contract takes care of deploying and tracking the different external coins (or
 * BYOC) that are present on PBC. Adding new coins takes place through the voting contract, meaning
 * that new external coins can only be added if a majority of the chain wishes.
 */
@AutoSysContract(ByocOrchestratorContractState.class)
public final class ByocOrchestratorContract {

  /** Invocation shortnames. */
  static final class Invocations {
    static final int UPDATE_CONTRACT_BUNDLE = 1;
    static final int ADD_BYOC = 2;
    static final int ADD_TOKEN_BRIDGE = 3;

    private Invocations() {}
  }

  /** Callback shortnames. */
  static final class Callbacks {
    static final int ADD_BYOC_CALLBACK = 2;

    static DataStreamSerializable addByocCallback(
        String symbol,
        String chain,
        BridgeParametersRpc bridgeParams,
        ExternalBlockchainAddressRpc externalAddress,
        PriceOracleConfiguration priceOracleConfiguration) {
      return rpc -> {
        rpc.writeByte(ADD_BYOC_CALLBACK);
        rpc.writeString(symbol);
        rpc.writeString(chain);
        bridgeParams.write(rpc);
        externalAddress.write(rpc);
        rpc.writeOptional(PriceOracleConfiguration::write, priceOracleConfiguration);
      };
    }

    private Callbacks() {}
  }

  /**
   * Initialize the BYOC orchestrator contract. The contract is initialized with information about
   * all existing bridges, as well as the code corresponding to the newest versions of byoc-incoming
   * and byoc-outgoing.
   *
   * <p>Part of the initialization of this contract will involve updating all existing BYOC
   * contracts to the contracts in {@code initialContractBundles}.
   *
   * @param context contract context
   * @param votingContract address of the voting contract
   * @param largeOracleContract address of the large oracle contract
   * @param initialContractBundles a list of initial {@link ContractBundle}s for the orchestrator
   * @param initialBridges a list of the existing BYOC bridges
   * @return the initial state
   */
  @Init
  public ByocOrchestratorContractState create(
      SysContractContext context,
      BlockchainAddress votingContract,
      BlockchainAddress largeOracleContract,
      List<ContractBundleRpc> initialContractBundles,
      List<SymbolAndBridgeRpc> initialBridges) {

    ByocOrchestratorContractState state =
        ByocOrchestratorContractState.createInitial(votingContract, largeOracleContract);

    for (SymbolAndBridgeRpc symbolAndBridge : initialBridges) {
      state = state.addBridge(symbolAndBridge.symbol(), symbolAndBridge.bridge().convert());
    }

    // Run through all the provided contracts and update the corresponding bridges. This also takes
    // care of storing the newest version of the different BYOC contracts.
    SystemEventCreator updater = context.getInvocationCreator();
    for (ContractBundleRpc bundleRpc : initialContractBundles) {
      ContractBundle bundle = bundleRpc.convert();

      List<BlockchainAddress> addresses = state.getContracts(bundleRpc.type());
      updateContracts(updater, addresses, bundle);
      state = state.updateContractBundle(bundleRpc.type(), bundle);
    }

    return state;
  }

  /**
   * Update the BYOC orchestrator contract to a new version.
   *
   * @param accessor a {@link StateAccessor} for the state of the previous version
   * @return the contract state after update
   */
  @Upgrade
  public ByocOrchestratorContractState upgrade(StateAccessor accessor) {
    return ByocOrchestratorContractState.migrateState(accessor);
  }

  /**
   * Add a new BYOC bridge. For this bridge to function, an external contract, on the external chain
   * this bridge connects to, must also be deployed.
   *
   * @param context contract context
   * @param state current state of this contract
   * @param coin the coin to add
   * @param bridgeParams parameters for the bridge
   * @param externalAddress address of the contract at the other end of the new bridge
   * @param priceOracleConfiguration optional configuration for deploying a price oracle contract
   * @return updated state
   */
  @Action(Invocations.ADD_BYOC)
  public ByocOrchestratorContractState addByoc(
      SysContractContext context,
      ByocOrchestratorContractState state,
      CoinRpc coin,
      BridgeParametersRpc bridgeParams,
      ExternalBlockchainAddressRpc externalAddress,
      @RpcType(nullable = true) PriceOracleConfiguration priceOracleConfiguration) {

    ensure(
        context.getFrom().equals(state.getVotingContract()),
        "Only the voting contract can add a BYOC bridge");

    // While legal to call SET_COIN on the account plugin with an existing symbol, we are only
    // interested in deploying new BYOC here.
    ensure(state.getBridge(coin.symbol()) == null, "Symbol already exists");

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.updateGlobalAccountPluginState(
        GlobalPluginStateUpdate.create(AccountPluginRpcHelper.setCoin(coin)));
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.addByocCallback(
            coin.symbol(), coin.chain(), bridgeParams, externalAddress, priceOracleConfiguration));

    return state;
  }

  /**
   * Update all BYOC contracts of a particular type. This invocation allows block producers (through
   * a majority vote) to update all bridges to a newer version. If the update succeeds, then the
   * provided contract bundle is stored in the state as the current version for that contract type.
   * This invocation can only be called by the voting contract.
   *
   * @param context contract context
   * @param state the current state
   * @param bundleRpc the new contract and what contract it is
   * @return updated state
   */
  @Action(Invocations.UPDATE_CONTRACT_BUNDLE)
  public ByocOrchestratorContractState updateBundle(
      SysContractContext context,
      ByocOrchestratorContractState state,
      ContractBundleRpc bundleRpc) {

    ensure(
        context.getFrom().equals(state.getVotingContract()),
        "Only the voting contract can update a contract");

    ContractBundle bundle = bundleRpc.convert();

    List<BlockchainAddress> addresses = state.getContracts(bundleRpc.type());
    SystemEventCreator invocationCreator = context.getInvocationCreator();
    updateContracts(invocationCreator, addresses, bundle);
    return state.updateContractBundle(bundleRpc.type(), bundle);
  }

  /**
   * Add a new token bridge to an existing BYOC on PBC. For this bridge to function, an external
   * contract, on the external chain this bridge connects to, must also be deployed.
   *
   * @param context the contract context
   * @param state the current contract state
   * @param coinSymbol the BYOC to add a new bridge for
   * @param chain the chain to bridge to
   * @param bridgeParams the withdrawal/deposit parameters for the new token bridge
   * @param externalAddress the address of the external withdraw/deposit contract for the new token
   *     bridge
   * @return an updated contract state with the new bridge added
   */
  @Action(Invocations.ADD_TOKEN_BRIDGE)
  public ByocOrchestratorContractState addTokenBridge(
      SysContractContext context,
      ByocOrchestratorContractState state,
      String coinSymbol,
      String chain,
      BridgeParametersRpc bridgeParams,
      ExternalBlockchainAddressRpc externalAddress) {
    boolean senderIsVotingContract = context.getFrom().equals(state.getVotingContract());
    ensure(
        senderIsVotingContract,
        "Only the voting contract can add a token bridge to an existing BYOC");

    boolean byocExists = state.getBridge(coinSymbol) != null;
    ensure(byocExists, "%s does not exist on PBC", coinSymbol);

    boolean tokenBridgeDoesNotAlreadyExist = state.getTokenBridge(coinSymbol, chain) == null;
    ensure(
        tokenBridgeDoesNotAlreadyExist,
        "Token bridge for coin %s and chain %s already exists",
        coinSymbol,
        chain);

    boolean tokenBridgeIsNotIdenticalToMainBridge =
        !state.getBridge(coinSymbol).chain().equals(chain);
    ensure(
        tokenBridgeIsNotIdenticalToMainBridge,
        "Can not deploy a token bridge for coin %s and chain %s when there already exists an"
            + " identical BYOC bridge",
        coinSymbol,
        chain);

    SystemEventCreator invocationCreator = context.getInvocationCreator();
    Hash txHash = context.getCurrentTransactionHash();

    BlockchainAddress incomingAddress =
        createAddress(BlockchainAddress.Type.CONTRACT_GOV, txHash, "deposit");
    deployIncoming(
        invocationCreator,
        incomingAddress,
        state.getContractBundle(ContractType.INCOMING),
        state.getLargeOracleContract(),
        coinSymbol,
        bridgeParams.depositMaximum());

    BlockchainAddress outgoingAddress =
        createAddress(BlockchainAddress.Type.CONTRACT_GOV, txHash, "withdraw");
    deployOutgoing(
        invocationCreator,
        outgoingAddress,
        state.getContractBundle(ContractType.OUTGOING),
        state.getLargeOracleContract(),
        externalAddress,
        coinSymbol,
        bridgeParams.withdrawMinimum(),
        bridgeParams.withdrawMaximum());

    return state.addTokenBridge(
        coinSymbol, new TokenBridge(chain, incomingAddress, outgoingAddress));
  }

  /**
   * Callback invoked when a new coin has successfully been set on the account plugin. This callback
   * takes care of deploying the incoming and outgoing smart contracts that contain the bridge
   * logic.
   *
   * <p>If the SET_COIN call was successful, then this method deploys two PBC contracts and adds a
   * new bridge to this contract's state. Otherwise, this callback does nothing.
   *
   * @param context contract context
   * @param state current state
   * @param callbackContext callback context
   * @param symbol the symbol for the new coin
   * @param chain a descriptor for the chain that we are bridging to
   * @param bridgeParams parameters for the bridge
   * @param externalAddress address for the contract on the remote chain
   * @param priceOracleConfiguration optional configuration for deploying a price oracle contract
   * @return updated state
   */
  @Callback(Callbacks.ADD_BYOC_CALLBACK)
  public ByocOrchestratorContractState addByocCallback(
      SysContractContext context,
      ByocOrchestratorContractState state,
      CallbackContext callbackContext,
      String symbol,
      String chain,
      BridgeParametersRpc bridgeParams,
      ExternalBlockchainAddressRpc externalAddress,
      @RpcType(nullable = true) PriceOracleConfiguration priceOracleConfiguration) {
    if (!callbackContext.isSuccess()) {
      return state;
    } else {
      SystemEventCreator invocationCreator = context.getInvocationCreator();
      Hash txHash = context.getCurrentTransactionHash();

      BlockchainAddress incomingAddress =
          createAddress(BlockchainAddress.Type.CONTRACT_GOV, txHash, "deposit");
      deployIncoming(
          invocationCreator,
          incomingAddress,
          state.getContractBundle(ContractType.INCOMING),
          state.getLargeOracleContract(),
          symbol,
          bridgeParams.depositMaximum());

      BlockchainAddress outgoingAddress =
          createAddress(BlockchainAddress.Type.CONTRACT_GOV, txHash, "withdraw");
      deployOutgoing(
          invocationCreator,
          outgoingAddress,
          state.getContractBundle(ContractType.OUTGOING),
          state.getLargeOracleContract(),
          externalAddress,
          symbol,
          bridgeParams.withdrawMinimum(),
          bridgeParams.withdrawMaximum());

      BlockchainAddress byocTokenAddress =
          createAddress(BlockchainAddress.Type.CONTRACT_SYSTEM, txHash, "byoc-token");
      deployByocToken(
          invocationCreator,
          byocTokenAddress,
          state.getContractBundle(ContractType.BYOC_TOKEN),
          symbol);

      BlockchainAddress priceOracleAddress = null;
      if (priceOracleConfiguration != null) {
        priceOracleAddress =
            createAddress(BlockchainAddress.Type.CONTRACT_GOV, txHash, "price-oracle");
        deployPriceOracle(
            invocationCreator,
            priceOracleAddress,
            state.getContractBundle(ContractType.PRICE_ORACLE),
            symbol,
            state.getLargeOracleContract(),
            priceOracleConfiguration);
      }

      return state.addBridge(
          symbol,
          new Bridge(
              chain,
              incomingAddress,
              outgoingAddress,
              byocTokenAddress,
              priceOracleAddress,
              FixedList.create()));
    }
  }

  /**
   * Deploy a byoc-incoming contract.
   *
   * @param invocationCreator an invocation creator used to deploy the contract
   * @param address the address to deploy the contract at
   * @param contractBundle the code for the contract
   * @param largeOracleContract argument used to initialize byoc-incoming
   * @param symbol argument used to initialize byoc-incoming
   * @param maximumDeposit argument used to initialize byoc-incoming
   */
  private static void deployIncoming(
      SystemEventCreator invocationCreator,
      BlockchainAddress address,
      ContractBundle contractBundle,
      BlockchainAddress largeOracleContract,
      String symbol,
      Unsigned256 maximumDeposit) {
    deployContract(
        invocationCreator,
        address,
        contractBundle,
        rpc -> {
          largeOracleContract.write(rpc);
          rpc.writeString(symbol);
          maximumDeposit.write(rpc);
        });
  }

  /**
   * Deploy a byoc-outgoing contract.
   *
   * @param invocationCreator invocation creator used to deploy the contract
   * @param address the address to deploy the contract at
   * @param contractBundle the code for the contract
   * @param largeOracleContract argument used to initialize byoc-outgoing
   * @param externalAddress argument used to initialize byoc-outgoing
   * @param symbol argument used to initialize byoc-outgoing
   * @param withdrawMinimum argument used to initialize byoc-outgoing
   * @param withdrawMaximum argument used to initialize byoc-outgoing
   */
  private static void deployOutgoing(
      SystemEventCreator invocationCreator,
      BlockchainAddress address,
      ContractBundle contractBundle,
      BlockchainAddress largeOracleContract,
      ExternalBlockchainAddressRpc externalAddress,
      String symbol,
      Unsigned256 withdrawMinimum,
      Unsigned256 withdrawMaximum) {
    deployContract(
        invocationCreator,
        address,
        contractBundle,
        rpc -> {
          largeOracleContract.write(rpc);
          externalAddress.write(rpc);
          rpc.writeString(symbol);
          rpc.writeInt(0); // empty List<BlockchainAddress>
          rpc.writeInt(0); // empty List<BlockchainPublicKey>
          withdrawMinimum.write(rpc);
          withdrawMaximum.write(rpc);
        });
  }

  /**
   * Deploy a byoc token contract.
   *
   * @param invocationCreator invocation creator used to deploy the contract
   * @param address the address to deploy the contract at
   * @param contractBundle the code for the contract
   * @param symbol argument used to initialize byoc-token
   */
  private void deployByocToken(
      SystemEventCreator invocationCreator,
      BlockchainAddress address,
      ContractBundle contractBundle,
      String symbol) {
    invocationCreator
        .deployContract(address)
        .allocateRemainingCost()
        .withContractJar(contractBundle.contract().getData())
        .withAbi(contractBundle.abi().getData())
        .withBinderJar(contractBundle.binder().getData())
        .withPayload(rpc -> rpc.writeString(symbol))
        .sendFromContract();
  }

  /**
   * Deploy a price oracle contract.
   *
   * @param invocationCreator invocation creator used to deploy the contract
   * @param address the address to deploy the contract at
   * @param contractBundle the code for the contract
   * @param symbol argument used to initialize price oracle
   * @param largeOracle argument used to initialize price oracle
   * @param priceOracleConfiguration configuration used to initialize price oracle
   */
  private void deployPriceOracle(
      SystemEventCreator invocationCreator,
      BlockchainAddress address,
      ContractBundle contractBundle,
      String symbol,
      BlockchainAddress largeOracle,
      PriceOracleConfiguration priceOracleConfiguration) {
    deployContract(
        invocationCreator,
        address,
        contractBundle,
        rpc -> {
          rpc.writeString(symbol);
          largeOracle.write(rpc);
          priceOracleConfiguration.write(rpc);
        });
  }

  /**
   * Helper method for creating new contract addresses. New contracts are given an address for a
   * governance contract derived from the hash <code>Hash(tx_hash || suffix)</code> where the suffix
   * can be varied to allow creation of different addresses for the same transaction hash.
   *
   * @param type type of contract to deploy
   * @param transactionHash a transaction hash
   * @param suffix a suffix
   * @return a deterministic address given {@code transactionHash} and suffix
   */
  private static BlockchainAddress createAddress(
      BlockchainAddress.Type type, Hash transactionHash, String suffix) {
    return BlockchainAddress.fromHash(
        type,
        Hash.create(
            stream -> {
              transactionHash.write(stream);
              stream.writeString(suffix);
            }));
  }

  /**
   * Deploys a contract.
   *
   * @param invocationCreator event creator
   * @param address the address of the contract to deploy
   * @param deploy the contract code (contract, binder and abi)
   * @param rpc initialization data for the contract
   */
  private static void deployContract(
      SystemEventCreator invocationCreator,
      BlockchainAddress address,
      ContractBundle deploy,
      DataStreamSerializable rpc) {
    invocationCreator
        .deployContract(address)
        .withContractJar(deploy.contract().getData())
        .withAbi(deploy.abi().getData())
        .withBinderJar(deploy.binder().getData())
        .withPayload(rpc)
        .allocateCost(0)
        .sendFromContract();
  }

  /**
   * Update a list of contracts of the same type to a new version.
   *
   * @param updater an event creator used to perform the updates
   * @param addresses a list of PBC addresses for the contracts to update
   * @param contractBundle code for the new version of the contract
   */
  private static void updateContracts(
      SystemEventCreator updater,
      List<BlockchainAddress> addresses,
      ContractBundle contractBundle) {
    byte[] rpc = new byte[0];
    for (BlockchainAddress address : addresses) {
      updater.upgradeSystemContract(
          contractBundle.contract().getData(),
          contractBundle.binder().getData(),
          contractBundle.abi().getData(),
          rpc,
          address);
    }
  }

  @FormatMethod
  private static void ensure(boolean predicate, String messageFormat, Object... args) {
    if (!predicate) {
      String errorMessage = String.format(messageFormat, args);
      throw new RuntimeException(errorMessage);
    }
  }
}
