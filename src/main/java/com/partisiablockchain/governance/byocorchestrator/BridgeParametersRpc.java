package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;

/**
 * Encapsulates the parameters that define the minimum and maximum amounts for withdrawals and
 * deposits on a given bridge. The maximum values define the upper bounds of coins that the bridge
 * will move before a new oracle is required. For withdraws, a minimum is provided as well because
 * withdraws on external chains are expensive.
 *
 * @param withdrawMaximum the maximum amount of BYOC the bridge will allow to withdraw per epoch
 * @param withdrawMinimum the minimum amount of BYOC the bridge will allow a user to withdraw
 * @param depositMaximum the maximum amount of BYOC the bridge will allow to deposit per epoch
 */
record BridgeParametersRpc(
    Unsigned256 withdrawMaximum, Unsigned256 withdrawMinimum, Unsigned256 depositMaximum) {

  void write(SafeDataOutputStream stream) {
    withdrawMaximum.write(stream);
    withdrawMinimum.write(stream);
    depositMaximum.write(stream);
  }
}
