package com.partisiablockchain.governance.byocorchestrator;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.tools.immutable.FixedList;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/** State class for the BYOC orchestrator contract. */
@Immutable
public final class ByocOrchestratorContractState implements StateSerializable {

  private final BlockchainAddress votingContract;
  private final BlockchainAddress largeOracleContract;

  /**
   * A mapping from a coin symbol to a bridge. Each bridge keeps track of a chain and the
   * corresponding incoming, outgoing and token contract, and an optional price oracle contract, as
   * well as any additional token bridges for this BYOC.
   */
  private final AvlTree<String, Bridge> byocBridges;

  /**
   * A mapping from {@link ContractType} to a {@link ContractBundle}. This map tells us the current
   * version of the different BYOC contracts and is used whenever a new BYOC is added.
   */
  private final AvlTree<ContractType, ContractBundle> contractBundles;

  @SuppressWarnings("unused")
  private ByocOrchestratorContractState() {
    votingContract = null;
    largeOracleContract = null;
    // silence warnings about these fields potentially being null
    byocBridges = AvlTree.create();
    contractBundles = AvlTree.create();
  }

  ByocOrchestratorContractState(
      BlockchainAddress votingContract,
      BlockchainAddress largeOracleContract,
      AvlTree<String, Bridge> byocBridges,
      AvlTree<ContractType, ContractBundle> contractBundles) {
    this.votingContract = votingContract;
    this.largeOracleContract = largeOracleContract;
    this.byocBridges = byocBridges;
    this.contractBundles = contractBundles;
  }

  /**
   * Create an initially empty contract state.
   *
   * @param votingContract address of the voting contract
   * @param largeOracleContract address of the large oracle contract
   * @return a ByocOrchestratorContractState object
   */
  static ByocOrchestratorContractState createInitial(
      BlockchainAddress votingContract, BlockchainAddress largeOracleContract) {
    return new ByocOrchestratorContractState(
        votingContract, largeOracleContract, AvlTree.create(), AvlTree.create());
  }

  static ByocOrchestratorContractState migrateState(StateAccessor accessor) {
    BlockchainAddress votingContract = accessor.get("votingContract").blockchainAddressValue();
    BlockchainAddress largeOracleContract =
        accessor.get("largeOracleContract").blockchainAddressValue();

    AvlTree<String, Bridge> byocBridges = migrateByocBridges(accessor.get("byocBridges"));

    AvlTree<ContractType, ContractBundle> contractBundles =
        migrateContractBundles(accessor.get("contractBundles"));

    return new ByocOrchestratorContractState(
        votingContract, largeOracleContract, byocBridges, contractBundles);
  }

  private static AvlTree<String, Bridge> migrateByocBridges(StateAccessor stateAccessor) {
    AvlTree<String, Bridge> migratedBridges = AvlTree.create();
    for (StateAccessorAvlLeafNode coinAndBridge : stateAccessor.getTreeLeaves()) {
      String coinSymbol = coinAndBridge.getKey().stringValue();
      Bridge bridge = Bridge.createFromStateAccessor(coinAndBridge.getValue());
      migratedBridges = migratedBridges.set(coinSymbol, bridge);
    }
    return migratedBridges;
  }

  @SuppressWarnings("EnumOrdinal")
  private static AvlTree<ContractType, ContractBundle> migrateContractBundles(
      StateAccessor accessor) {
    AvlTree<ContractType, ContractBundle> map = AvlTree.create();
    for (StateAccessorAvlLeafNode treeLeaf : accessor.getTreeLeaves()) {
      Enum<?> type = (Enum<?>) treeLeaf.getKey().cast(Enum.class);
      ContractBundle contractBundle = ContractBundle.createFromStateAccessor(treeLeaf.getValue());
      map = map.set(ContractType.values()[type.ordinal()], contractBundle);
    }
    return map;
  }

  BlockchainAddress getVotingContract() {
    return votingContract;
  }

  BlockchainAddress getLargeOracleContract() {
    return largeOracleContract;
  }

  /**
   * Gets the bridge for the given BYOC.
   *
   * @param coinSymbol the BYOC to find the bridge for
   * @return a bridge or null if no bridge was found
   */
  Bridge getBridge(String coinSymbol) {
    return byocBridges.getValue(coinSymbol);
  }

  /**
   * Gets the token bridges for the given BYOC.
   *
   * @param coinSymbol the BYOC to find the token bridges for
   * @return the list of token bridges or an empty list if no token bridges were found
   */
  FixedList<TokenBridge> getTokenBridges(String coinSymbol) {
    return getBridge(coinSymbol).tokenBridges();
  }

  /**
   * Gets the token bridge for a given BYOC and external chain.
   *
   * @param coinSymbol the BYOC to get the token bridge for
   * @param chain the external chain the token bridge is associated with
   * @return the token bridge or null if no token bridge were found
   */
  TokenBridge getTokenBridge(String coinSymbol, String chain) {
    for (TokenBridge tokenBridge : getTokenBridges(coinSymbol)) {
      if (tokenBridge.chain().equals(chain)) {
        return tokenBridge;
      }
    }
    return null;
  }

  /**
   * Get all the contract addresses corresponding to a particular {@link ContractType}.
   *
   * @param type the contract type
   * @return a list of PBC addresses
   */
  List<BlockchainAddress> getContracts(ContractType type) {
    ArrayList<BlockchainAddress> contracts = new ArrayList<>(getByocBridgeContracts(type));
    contracts.addAll(getTokenBridgeContracts(type));
    return contracts;
  }

  /**
   * Get the contract addresses corresponding to a particular {@link ContractType} on the BYOC
   * bridges.
   *
   * @param type the contract type
   * @return a list of PBC addresses
   */
  List<BlockchainAddress> getByocBridgeContracts(ContractType type) {
    if (type == ContractType.INCOMING) {
      return byocBridges.values().stream().map(Bridge::incoming).toList();
    } else if (type == ContractType.OUTGOING) {
      return byocBridges.values().stream().map(Bridge::outgoing).toList();
    } else if (type == ContractType.BYOC_TOKEN) {
      return byocBridges.values().stream().map(Bridge::byocToken).toList();
    } else if (type == ContractType.PRICE_ORACLE) {
      return byocBridges.values().stream()
          .map(Bridge::priceOracle)
          .filter(Objects::nonNull)
          .toList();
    } else {
      throw new IllegalArgumentException("Invalid contract type");
    }
  }

  /**
   * Get the contract addresses corresponding to a particular {@link ContractType} on the token
   * bridges.
   *
   * @param type the contract type
   * @return a list of PBC addresses
   */
  List<BlockchainAddress> getTokenBridgeContracts(ContractType type) {
    if (type == ContractType.INCOMING) {
      return byocBridges.values().stream()
          .map(Bridge::tokenBridges)
          .flatMap(FixedList::stream)
          .map(TokenBridge::incoming)
          .toList();
    } else if (type == ContractType.OUTGOING) {
      return byocBridges.values().stream()
          .map(Bridge::tokenBridges)
          .flatMap(FixedList::stream)
          .map(TokenBridge::outgoing)
          .toList();
    } else {
      // Token bridges do not hold price oracle or token contracts
      return List.of();
    }
  }

  ContractBundle getContractBundle(ContractType type) {
    return contractBundles.getValue(type);
  }

  /**
   * Add a new bridge to the state.
   *
   * @param symbol the coin symbol
   * @param bridge the bridge
   * @return updated state
   */
  public ByocOrchestratorContractState addBridge(String symbol, Bridge bridge) {
    AvlTree<String, Bridge> updatedBridges = byocBridges.set(symbol, bridge);
    return new ByocOrchestratorContractState(
        votingContract, largeOracleContract, updatedBridges, contractBundles);
  }

  /**
   * Adds a token bridge to the BYOC bridge of the given BYOC.
   *
   * @param coinSymbol the BYOC to add a token bridge for
   * @param tokenBridge the token bridge to add
   * @return an updated contract state with the token bridge added
   */
  ByocOrchestratorContractState addTokenBridge(String coinSymbol, TokenBridge tokenBridge) {
    Bridge existingBridge = byocBridges.getValue(coinSymbol);
    FixedList<TokenBridge> updatedTokenBridges =
        existingBridge.tokenBridges().addElement(tokenBridge);
    Bridge updatedBridge =
        new Bridge(
            existingBridge.chain(),
            existingBridge.incoming(),
            existingBridge.outgoing(),
            existingBridge.byocToken(),
            existingBridge.priceOracle(),
            updatedTokenBridges);

    return new ByocOrchestratorContractState(
        votingContract,
        largeOracleContract,
        byocBridges.set(coinSymbol, updatedBridge),
        contractBundles);
  }

  /**
   * Update the contract bundle for a BYOC contract type.
   *
   * @param type the contract type
   * @param bundle the new contract bundle
   * @return updated state
   */
  public ByocOrchestratorContractState updateContractBundle(
      ContractType type, ContractBundle bundle) {
    return new ByocOrchestratorContractState(
        votingContract, largeOracleContract, byocBridges, contractBundles.set(type, bundle));
  }
}
